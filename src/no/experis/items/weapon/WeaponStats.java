package no.experis.items.weapon;

//Keeps track of armor attributes
public class WeaponStats
{
    public int BaseDamage;
    public int Scaling;
    public double HeroStatScaling;
    public WeaponStats(int baseDamage, int scaling, double heroStatScaling)
    {
        BaseDamage = baseDamage;
        Scaling = scaling;
        HeroStatScaling = heroStatScaling;
    }
    //Scales up any weapon with its scaling value.
    public void ScaleUp()
    {
        BaseDamage += Scaling;
    }
    //Scales down any weapon with its scaling value.
    public void ScaleDown()
    {
        BaseDamage -= Scaling;
    }
}
