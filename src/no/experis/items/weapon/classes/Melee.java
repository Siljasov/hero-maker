package no.experis.items.weapon.classes;

import no.experis.items.weapon.Weapon;
import no.experis.items.weapon.WeaponStats;
import no.experis.items.weapon.WeaponType;

//Default melee weapon with its default values
public class Melee extends Weapon
{
    public Melee(String name)
    {
        super(name, WeaponType.Melee, new WeaponStats(15, 2, 1.5));
    }
    public Melee(String name, int desiredLevel)
    {
        super(name, WeaponType.Melee, new WeaponStats(15, 2, 1.5), desiredLevel);
    }

}
