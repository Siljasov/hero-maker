package no.experis.items.weapon.classes;

import no.experis.items.weapon.Weapon;
import no.experis.items.weapon.WeaponStats;
import no.experis.items.weapon.WeaponType;

//Default ranged weapon with its default values
public class Ranged extends Weapon
{
    public Ranged(String name)
    {
        super(name, WeaponType.Ranged, new WeaponStats(5,3,2));
    }
    public Ranged(String name, int desiredLevel)
    {
        super(name, WeaponType.Ranged, new WeaponStats(5,3,2), desiredLevel);
    }
}
