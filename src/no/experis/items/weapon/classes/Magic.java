package no.experis.items.weapon.classes;

import no.experis.items.weapon.Weapon;
import no.experis.items.weapon.WeaponStats;
import no.experis.items.weapon.WeaponType;

//Default magic weapon with its default values
public class Magic extends Weapon
{
    public Magic(String name)
    {
        super(name, WeaponType.Magic, new WeaponStats(25, 2, 3));
    }
    public Magic(String name, int desiredLevel)
    {
        super(name, WeaponType.Magic, new WeaponStats(25, 2, 3), desiredLevel);
    }
}
