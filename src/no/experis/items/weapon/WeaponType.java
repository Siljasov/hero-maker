package no.experis.items.weapon;

//Keeps track of all weapon types. More can be added here
public enum WeaponType
{
    Melee, Ranged, Magic
}
