package no.experis.items.weapon;

import no.experis.hero.HeroStats;
import no.experis.level.WeaponLevel;

//This is the main Weapon class that all weapons inherit from. All weapon related actions happen here
public class Weapon
{
    public String Name;
    public WeaponType Type;
    public WeaponStats Stats;
    public WeaponLevel Level;
    public Weapon(String name, WeaponType type, WeaponStats stats)
    {
        Name = name;
        Type = type;
        Stats = stats;
        Level = new WeaponLevel();
        Level.SetLevel(Stats, 1);
    }
    public Weapon(String name, WeaponType type, WeaponStats stats, int level)
    {
        Name = name;
        Type = type;
        Stats = stats;
        Level = new WeaponLevel();
        Level.SetLevel(Stats, level);
    }
    public void LevelUp()
    {
        Level.LevelUp(Stats);
    }
    public void SetLevel(int desiredLevel)
    {
        Level.SetLevel(Stats, desiredLevel);
    }
    //The final damage for the hero is calculated here
    public int GetDamage(HeroStats heroStats)
    {
        switch (Type)
        {
            case Melee: return (int)(Stats.BaseDamage + (Stats.HeroStatScaling * heroStats.Strength));
            case Ranged: return (int)(Stats.BaseDamage + (Stats.HeroStatScaling * heroStats.Dexterity));
            case Magic: return (int)(Stats.BaseDamage + (Stats.HeroStatScaling * heroStats.Intelligence));
            default: throw new IllegalStateException("Unexpected weapon type: " + Type);
        }
    }
}
