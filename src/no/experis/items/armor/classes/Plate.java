package no.experis.items.armor.classes;

import no.experis.items.armor.Armor;
import no.experis.items.armor.ArmorStats;
import no.experis.items.armor.ArmorType;

//Default plate armor item with its default values
public class Plate extends Armor
{
    public Plate(String name)
    {
        super(name, ArmorType.Plate, new ArmorStats(30, 3, 1, 0, 12, 2, 1, 0));
    }
}
