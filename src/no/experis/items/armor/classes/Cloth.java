package no.experis.items.armor.classes;

import no.experis.items.armor.Armor;
import no.experis.items.armor.ArmorStats;
import no.experis.items.armor.ArmorType;

//Default cloth armor item with its default values
public class Cloth extends Armor
{
    public Cloth(String name)
    {
        super(name, ArmorType.Cloth, new ArmorStats(10, 0, 1, 3, 5, 0, 1, 2));
    }
}
