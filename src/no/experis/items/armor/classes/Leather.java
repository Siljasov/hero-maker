package no.experis.items.armor.classes;

import no.experis.items.armor.Armor;
import no.experis.items.armor.ArmorStats;
import no.experis.items.armor.ArmorType;

//Default leather armor item with its default values
public class Leather extends Armor
{
    public Leather(String name)
    {
        super(name, ArmorType.Leather, new ArmorStats(20, 1, 3, 0, 8, 2, 1, 0));
    }
}
