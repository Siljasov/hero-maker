package no.experis.items.armor;

//Keeps track of armor attributes
public class ArmorStats
{
    public int BaseHealth = 0;
    public int BaseStrength = 0;
    public int BaseDexterity = 0;
    public int BaseIntelligence = 0;
    public int ScalingHealth = 0;
    public int ScalingStrength = 0;
    public int ScalingDexterity = 0;
    public int ScalingIntelligence = 0;
    public ArmorStats(int baseHealth, int baseStrength, int baseDexterity, int baseIntelligence, int scalingHealth, int scalingStrength, int scalingDexterity, int scalingIntelligence)
    {
        BaseHealth = baseHealth;
        BaseStrength = baseStrength;
        BaseDexterity = baseDexterity;
        BaseIntelligence = baseIntelligence;
        ScalingHealth = scalingHealth;
        ScalingStrength = scalingStrength;
        ScalingDexterity = scalingDexterity;
        ScalingIntelligence = scalingIntelligence;
    }
    //Scales up any armor item with its scaling value.
    public void ScaleUp()
    {
        BaseHealth += ScalingHealth;
        BaseStrength += ScalingStrength;
        BaseDexterity += ScalingDexterity;
        BaseIntelligence += ScalingIntelligence;
    }
    //Scales down any armor item with its scaling value.
    public void ScaleDown()
    {
        BaseHealth += ScalingHealth;
        BaseStrength += ScalingStrength;
        BaseDexterity += ScalingDexterity;
        BaseIntelligence += ScalingIntelligence;
    }

}
