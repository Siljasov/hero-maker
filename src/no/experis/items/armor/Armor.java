package no.experis.items.armor;

import no.experis.hero.Hero;
import no.experis.level.ArmorLevel;

//This is the main Armor class that all armor items inherit from. All armor related actions happen here
public class Armor
{
    public String Name;
    public ArmorType Type;
    public ArmorStats Stats;
    public ArmorLevel Level;
    public Armor(String name, ArmorType type, ArmorStats stats)
    {
        Name = name;
        Type = type;
        Stats = stats;
        Level = new ArmorLevel();
        Level.SetLevel(Stats, 1);
    }
    public Armor(String name, ArmorType type, ArmorStats stats, int level)
    {
        Name = name;
        Type = type;
        Stats = stats;
        Level = new ArmorLevel();
        Level.SetLevel(Stats, level);
    }
    public void LevelUp()
    {
        Level.LevelUp(Stats);
    }
    public void AddBonus(Hero hero, double scaling)
    {
        hero.Stats.Health = (int)((hero.Stats.Health + Stats.BaseHealth) * scaling);
        hero.Stats.Strength = (int)((hero.Stats.Strength + Stats.BaseStrength) * scaling);
        hero.Stats.Dexterity = (int)((hero.Stats.Dexterity + Stats.BaseDexterity) * scaling);
        hero.Stats.Intelligence = (int)((hero.Stats.Intelligence + Stats.BaseIntelligence) * scaling);
    }
    public void RemoveBonus(Hero hero, double scaling)
    {
        hero.Stats.Health = (int)((hero.Stats.Health - Stats.BaseHealth) * scaling);
        hero.Stats.Strength = (int)((hero.Stats.Strength - Stats.BaseStrength) * scaling);
        hero.Stats.Dexterity = (int)((hero.Stats.Dexterity - Stats.BaseDexterity) * scaling);
        hero.Stats.Intelligence = (int)((hero.Stats.Intelligence - Stats.BaseIntelligence) * scaling);
    }
    public void SetLevel(int desiredLevel)
    {
        Level.SetLevel(Stats, desiredLevel);
    }
}
