package no.experis.items.armor;

//All armor types are stored in this enum. More can be added here
public enum ArmorType
{
    Cloth, Leather, Plate
}
