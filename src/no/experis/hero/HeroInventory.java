package no.experis.hero;

import no.experis.items.armor.Armor;
import no.experis.items.weapon.Weapon;

//This class is responsible for keeping all items that the hero has and manipulating the inventory
public class HeroInventory
{
    private Weapon HandWeapon = null;
    private Armor Head = null;
    private Armor Body = null;
    private Armor Legs = null;
    public HeroInventory() {}
    public HeroInventory(Weapon weapon, Armor head, Armor body, Armor legs)
    {
        HandWeapon = weapon;
        Head = head;
        Body = body;
        Legs = legs;
    }
    public Weapon GetHandWeapon()
    {
        return HandWeapon;
    }

    public void SetHandWeapon(Weapon handWeapon)
    {
        HandWeapon = handWeapon;
    }

    public void RemoveHandWeapon(Hero hero) { HandWeapon = null; }

    public Armor GetHead()
    {
        return Head;
    }

    public void SetHead(Armor head, Hero hero)
    {
        if (head == null || hero == null) return;
        Head = head;
        head.AddBonus(hero, 1);
    }

    public void RemoveHead(Hero hero)
    {
        Head.RemoveBonus(hero,1);
        Head = null;
    }

    public Armor GetBody() {
        return Body;
    }

    public void SetBody(Armor body, Hero hero)
    {
        if (body == null || hero == null) return;
        Body = body;
        body.AddBonus(hero, 0.8);
    }

    public void RemoveBody(Hero hero)
    {
        Body.RemoveBonus(hero,0.8);
        Body = null;
    }

    public Armor GetLegs()
    {
        return Legs;
    }

    public void SetLegs(Armor legs, Hero hero)
    {
        if (legs == null || hero == null) return;;
        Legs = legs;
        legs.AddBonus(hero, 0.6);
    }

    public void RemoveLegs(Hero hero)
    {
        Legs.RemoveBonus(hero,0.6);
        Legs = null;
    }
}
