package no.experis.hero;

//This enum keeps all possible hero classes. More can be added by including them here
public enum HeroType
{
    Warrior, Ranger, Mage
}