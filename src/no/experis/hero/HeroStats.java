package no.experis.hero;

//This class keeps track of the hero's main attributes
public class HeroStats
{
    public int Health = 0;
    public int Strength = 0;
    public int Dexterity = 0;
    public int Intelligence = 0;
    public int ScalingHealth = 0;
    public int ScalingStrength = 0;
    public int ScalingDexterity = 0;
    public int ScalingIntelligence = 0;
    public HeroStats(int health, int strength, int dexterity, int intelligence, int scalingHealth, int scalingStrength, int scalingDexterity, int scalingIntelligence)
    {
        Health = health;
        Strength = strength;
        Dexterity = dexterity;
        Intelligence = intelligence;
        ScalingHealth = scalingHealth;
        ScalingStrength = scalingStrength;
        ScalingDexterity = scalingDexterity;
        ScalingIntelligence = scalingIntelligence;
    }
    //Increases the hero's attributes. This function's primary caller is HeroLevel.LevelUp()
    public void ScaleUp()
    {
        Health += ScalingHealth;
        Strength += ScalingStrength;
        Dexterity += ScalingDexterity;
        Intelligence += ScalingIntelligence;
    }
}
