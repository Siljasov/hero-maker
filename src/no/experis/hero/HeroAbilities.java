package no.experis.hero;

import no.experis.items.weapon.Weapon;

//This class contains all abilities that the hero can/could do
//If we want a jump ability in the future, it's as simple as adding the jump implementation here
public class HeroAbilities
{
    public int Attack(Weapon weapon, HeroStats stats)
    {
        if (weapon == null) return 0;
        return weapon.GetDamage(stats);
    }
}
