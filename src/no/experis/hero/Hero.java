package no.experis.hero;

import no.experis.items.armor.Armor;
import no.experis.items.weapon.Weapon;
import no.experis.level.HeroLevel;

//The main hero class. It's responsible for containing all primary hero related functions. The default hero classes inherit from this class
public class Hero
{
    public String Name;
    private HeroType Type;
    public HeroStats Stats;
    protected HeroInventory Inventory;
    public HeroLevel Level;
    public HeroAbilities Abilities;
    public Hero(String name, HeroType type, HeroStats stats)
    {
        Name = name;
        Type = type;
        Stats = stats;
        Inventory = new HeroInventory();
        Level = new HeroLevel();
        Abilities = new HeroAbilities();
    }
    public void LevelUp()
    {
        Level.LevelUp(Stats);
    }
    public void SetLevel(int desiredLevel)
    {
        Level.SetLevel(Stats, desiredLevel);
    }
    public void GiveXP(int amount)
    {
        Level.GiveExperience(Stats, amount);
    }
    public boolean SetWeapon(Weapon weapon)
    {
        if (weapon == null) return false;
        if (weapon.Level.CurrentLevel <= Level.CurrentLevel)
        {
            Inventory.SetHandWeapon(weapon);
            return true;
        } else return false;
    }
    public boolean RemoveWeapon()
    {
        if (Inventory.GetHandWeapon() != null)
        {
            Inventory.RemoveHandWeapon(this);
            return true;
        } else return false;
    }
    public boolean SetHeadArmor(Armor armor)
    {
        if (armor == null) return false;
        if (armor.Level.CurrentLevel <= Level.CurrentLevel)
        {
            Inventory.SetHead(armor, this);
            return true;
        } else return false;
    }
    public boolean SetBodyArmor(Armor armor)
    {
        if (armor == null) return false;
        if (armor.Level.CurrentLevel <= Level.CurrentLevel)
        {
            Inventory.SetBody(armor, this);
            return true;
        } else return false;
    }
    public boolean SetLegsArmor(Armor armor)
    {
        if (armor == null) return false;
        if (armor.Level.CurrentLevel <= Level.CurrentLevel)
        {
            Inventory.SetLegs(armor, this);
            return true;
        } else return false;
    }
    public boolean RemoveHeadArmor()
    {
        if (Inventory.GetHead() != null)
        {
            Inventory.RemoveHead(this);
            return true;
        } else return false;
    }
    public boolean RemoveBodyArmor()
    {
        if (Inventory.GetBody() != null)
        {
            Inventory.RemoveBody(this);
            return true;
        } else return false;
    }
    public boolean RemoveLegsArmor()
    {
        if (Inventory.GetLegs() != null)
        {
            Inventory.RemoveLegs(this);
            return true;
        } else return false;
    }
    public int Attack()
    {
        return Abilities.Attack(Inventory.GetHandWeapon(), Stats);
    }
    public HeroInventory GetInventory()
    {
        return Inventory;
    }
}
