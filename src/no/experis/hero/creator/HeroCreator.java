package no.experis.hero.creator;

import no.experis.hero.Hero;

//This class is responsible for creating a new character and manipulating it
public class HeroCreator
{
    Hero Character;
    HeroCreatorPrinter printer;
    public HeroCreator()
    {
        Character = null;
        printer = new HeroCreatorPrinter();
    }
    //Creates a default character from a list and runs the main menu loop.
    public void Run()
    {
        printer.Welcome();
        Character = printer.ClassSelect();
        printer.Menu(Character);
    }
}
