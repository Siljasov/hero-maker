package no.experis.hero.creator;

import no.experis.hero.Hero;
import no.experis.hero.classes.Mage;
import no.experis.hero.classes.Ranger;
import no.experis.hero.classes.Warrior;
import no.experis.items.armor.Armor;
import no.experis.items.armor.classes.Cloth;
import no.experis.items.armor.classes.Leather;
import no.experis.items.armor.classes.Plate;
import no.experis.items.weapon.Weapon;
import no.experis.items.weapon.classes.Magic;
import no.experis.items.weapon.classes.Melee;
import no.experis.items.weapon.classes.Ranged;
import no.experis.visual.VisualPrinter;

import java.util.Scanner;

//This class is responsible for driving the main UI loop and carrying out various hero tasks
public class HeroCreatorPrinter
{
    Scanner console = new Scanner(System.in);
    public void Welcome()
    {
        System.out.println("****Welcome the the Hero Creator!****\nLet's start by creating your own hero.");
    }
    //Allows the user to select a class and returns the newly created character for HeroCreator
    public Hero ClassSelect()
    {
        System.out.println("Select your hero class:");
        System.out.println("1. Warrior");
        System.out.println("2. Ranger");
        System.out.println("3. Mage");
        int selection = GetInput(1,3);
        System.out.println("Character created!");
        Wait(2);
        switch (selection)
        {
            case 1: return new Warrior();
            case 2: return new Ranger();
            case 3: return new Mage();
            default: return null;
        }
    }
    //Tests all input. Should cover most cases
    private int GetInput(int min, int max)
    {
        while(true)
        {
            int selection = 0;
            System.out.print("Input: ");
            if (console.hasNextInt())
            {
                selection = console.nextInt();
                if (selection >= min && selection <= max) return selection;
                else System.out.println("Invalid range!");
            }
            else { System.out.println("invalid characters!"); console.next(); }
        }
    }
    //The main game menu. All the big commands are found here
    public void Menu(Hero hero)
    {
        boolean keepLooping = true;
        while (keepLooping)
        {
            System.out.println("+------+");
            System.out.println("| Menu |");
            System.out.println("+------+");
            System.out.println("1. Show hero details");
            System.out.println("2. Show hero inventory");
            System.out.println("3. Set item to hero");
            System.out.println("4. Remove item from hero");
            System.out.println("5. Add hero XP");
            System.out.println("6. Set item level");
            System.out.println("7. Attack!");
            System.out.println("8. Exit");
            int selection = GetInput(1,8);
            switch (selection)
            {
                case 1: VisualPrinter.PrintHero(hero); break;
                case 2: VisualPrinter.PrintHeroInventory(hero); break;
                case 3: ChangeHeroItem(hero, false); break;
                case 4: ChangeHeroItem(hero, true); break;
                case 5: AddHeroXP(hero); break;
                case 6: AddItemXP(hero); break;
                case 7: VisualPrinter.PrintHeroAttack(hero); break;
                case 8: keepLooping = false; System.out.println("Exiting..."); break;
            }
            Wait(2);
        }
    }
    private void AddHeroXP(Hero hero)
    {
        System.out.println("Enter amount of XP to add: ");
        int amount = GetInput(0,100000);
        hero.GiveXP(amount);
        System.out.println("Gave the hero " + amount + " experience points!");
    }
    //Adds XP to a particular inventory item
    private void AddItemXP(Hero hero)
    {
        System.out.println("1. Weapon item");
        System.out.println("2. Head item");
        System.out.println("3. Body item");
        System.out.println("4. Legs item");
        int selection = GetInput(1,4);
        switch (selection)
        {
            case 1:
                if (hero.GetInventory().GetHandWeapon() != null)
                {
                    System.out.println("Enter desired level");
                    int level = GetInput(1, 99);
                    System.out.println("Item level modified!");
                    hero.GetInventory().GetHandWeapon().SetLevel(level);
                }
                else System.out.println("You don't have a weapon!");
                break;
            case 2:
                if (hero.GetInventory().GetHead() != null)
                {
                    System.out.println("Enter desired level");
                    int level = GetInput(1, 99);
                    System.out.println("Item level modified!");
                    hero.GetInventory().GetHead().SetLevel(level);
                }
                else System.out.println("You don't have any head armor!");
                break;
            case 3:
                if (hero.GetInventory().GetBody() != null)
                {
                    System.out.println("Enter desired level");
                    int level = GetInput(1, 99);
                    System.out.println("Item level modified!");
                    hero.GetInventory().GetBody().SetLevel(level);
                }
                else System.out.println("You don't have any body armor!");
                break;
            case 4:
                if (hero.GetInventory().GetLegs() != null)
                {
                    System.out.println("Enter desired level");
                    int level = GetInput(1, 99);
                    System.out.println("Item level modified!");
                    hero.GetInventory().GetLegs().SetLevel(level);
                }
                else System.out.println("You don't have any leg armor!");
                break;
        }
    }
    //Either adds or removes a specific item to the hero's inventory. This is controlled by "remove" variable
    private void ChangeHeroItem(Hero hero, boolean remove)
    {
        System.out.println("1. Weapon slot");
        System.out.println("2. Head slot");
        System.out.println("3. Body  slot");
        System.out.println("4. Legs slot");
        int selection = GetInput(1,4);
        switch (selection)
        {
            case 1:
                if (remove) { System.out.println("Weapon removed!"); hero.RemoveWeapon(); }
                else hero.SetWeapon(AddHeroWeapon());
                break;
            case 2:
                if (remove) { System.out.println("Head armor removed!"); hero.RemoveHeadArmor(); }
                else hero.SetHeadArmor(AddHeroArmor());
                break;
            case 3:
                if (remove) { System.out.println("Body armor removed!");hero.RemoveBodyArmor(); }
                else hero.SetBodyArmor(AddHeroArmor());
                break;
            case 4:
                if (remove) { System.out.println("Leg armor removed!");hero.RemoveLegsArmor(); }
                else hero.SetLegsArmor(AddHeroArmor());
                break;
        }
    }
    //Gets called by AddHeroItem. Adds a weapon
    private Weapon AddHeroWeapon()
    {
        System.out.println("1. Melee weapon");
        System.out.println("2. Ranged weapon");
        System.out.println("3. Magic weapon");
        int selection = GetInput(1,3);
        System.out.println("Weapon slot modified!");
        switch (selection)
        {
            case 1: return new Melee("Blade of the Swordsman");
            case 2: return new Ranged("Bow of Bone");
            case 3: return new Magic("The Undead Staff");
            default: return null;
        }
    }
    //Gets called by AddHeroItem. Adds armor
    private Armor AddHeroArmor()
    {
        System.out.println("1. Cloth armor");
        System.out.println("2. Leather armor");
        System.out.println("3. Plate armor");
        int selection = GetInput(1,3);
        System.out.println("Armor slot modified!");
        switch (selection)
        {
            case 1: return new Cloth("Armor of the Forest");
            case 2: return new Leather("Hide Armor of Cursed Souls");
            case 3: return new Plate("Legionnaire's Titanium Armor");
            default: return null;
        }
    }
    private void Wait(int seconds)
    {
        try
        {
            Thread.sleep(seconds * 1000);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
}
