package no.experis.hero.classes;

import no.experis.hero.Hero;
import no.experis.hero.HeroStats;
import no.experis.hero.HeroType;

//Creates a default Mage character
public class Mage extends Hero
{
    public Mage()
    {
        super("Mage", HeroType.Mage, new HeroStats(100, 2, 3, 10, 15, 1, 2, 5));
    }
}
