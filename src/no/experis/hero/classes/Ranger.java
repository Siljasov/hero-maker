package no.experis.hero.classes;

import no.experis.hero.Hero;
import no.experis.hero.HeroStats;
import no.experis.hero.HeroType;

//Creates a default Ranger character
public class Ranger extends Hero
{
    public Ranger()
    {
        super("Ranger", HeroType.Ranger, new HeroStats(120, 5, 10, 2, 20, 2, 5, 1));
    }
}
