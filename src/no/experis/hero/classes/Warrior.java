package no.experis.hero.classes;

import no.experis.hero.Hero;
import no.experis.hero.HeroStats;
import no.experis.hero.HeroType;

//Creates a default Warrior character
public class Warrior extends Hero
{
    public Warrior()
    {
        super("Warrior", HeroType.Warrior, new HeroStats(150, 10, 3, 1,30,5, 2, 1));
    }
}
