package no.experis;

import no.experis.hero.creator.HeroCreator;

public class Main
{
    public static void main(String[] args)
    {
        HeroCreator creator = new HeroCreator();
        creator.Run();
    }
}
