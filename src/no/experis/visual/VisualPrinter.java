package no.experis.visual;

import no.experis.hero.Hero;
import no.experis.items.armor.Armor;
import no.experis.items.weapon.Weapon;

public class VisualPrinter
{
    public static void PrintHero(Hero HeroToPrint)
    {
        if (HeroToPrint == null) {System.out.println("No character to print!"); return;}
        System.out.println("+--------------+" +
                "\n| Hero Details |" +
                "\n+--------------+" +
                "\nName: " + HeroToPrint.Name +
                "\nHealth: " + HeroToPrint.Stats.Health +
                "\nStrength: " + HeroToPrint.Stats.Strength +
                "\nDexterity: " + HeroToPrint.Stats.Dexterity +
                "\nIntelligence: " + HeroToPrint.Stats.Intelligence +
                "\nLevel: " + HeroToPrint.Level.CurrentLevel + " (" + HeroToPrint.Level.ExperiencePoints + "/" + HeroToPrint.Level.ExperienceRequiredToLevel + ")\n");
    }
    public static void PrintHeroInventory(Hero HeroToPrint)
    {
        if (HeroToPrint == null) {System.out.println("No character to print!"); return;}
        System.out.println("+----------------+" +
                "\n| Hero Inventory |" +
                "\n+----------------+" +
                "\nWeapon: " + ((HeroToPrint.GetInventory().GetHandWeapon() != null) ? HeroToPrint.GetInventory().GetHandWeapon().Name + " (Level " + HeroToPrint.GetInventory().GetHandWeapon().Level.CurrentLevel + ")" : "None") +
                "\nHead: " + ((HeroToPrint.GetInventory().GetHead() != null) ? HeroToPrint.GetInventory().GetHead().Name + " (Level " + HeroToPrint.GetInventory().GetHead().Level.CurrentLevel + ")": "None") +
                "\nBody: " + ((HeroToPrint.GetInventory().GetBody() != null) ? HeroToPrint.GetInventory().GetBody().Name + " (Level " + HeroToPrint.GetInventory().GetBody().Level.CurrentLevel + ")": "None") +
                "\nLegs: " + ((HeroToPrint.GetInventory().GetLegs() != null) ? HeroToPrint.GetInventory().GetLegs().Name + " (Level " + HeroToPrint.GetInventory().GetLegs().Level.CurrentLevel + ")": "None") + "\n");
    }
    public static void PrintHeroAttack(Hero HeroToPrint)
    {
        if (HeroToPrint == null) {System.out.println("No hero to print!"); return;}
        System.out.println(HeroToPrint.Name + " has attacked for " + HeroToPrint.Attack() + " damage!\n");
    }
    //Prints any any item found in the hero's inventory
    public static <T> void PrintItem(T item)
    {
        if (item == null) {System.out.println("No item to print!"); return;}
        if (item instanceof Armor)
        {
            Armor armor = ((Armor)item);
            System.out.println("+--------------+" +
                    "\n| Armor Details |" +
                    "\n+--------------+" +
                    "\nName: " + armor.Name +
                    "\nType: " + armor.Type.toString() +
                    "\nLevel: " + armor.Level.CurrentLevel +
                    "\nBonus Health: " + armor.Stats.BaseHealth +
                    "\nBonus Strength: " + armor.Stats.BaseStrength +
                    "\nBonus Dexterity: " + armor.Stats.BaseDexterity +
                    "\nBonus Intelligence: " + armor.Stats.BaseIntelligence + "\n");
        }
        else if (item instanceof Weapon)
        {
            Weapon weapon = ((Weapon)item);
            System.out.println("+--------------+" +
                    "\n| Weapon Details |" +
                    "\n+--------------+" +
                    "\nName: " + weapon.Name +
                    "\nType: " + weapon.Type.toString() +
                    "\nLevel: " + weapon.Level.CurrentLevel +
                    "\nDamage: " + weapon.Stats.BaseDamage + "\n");
        }
    }

}

