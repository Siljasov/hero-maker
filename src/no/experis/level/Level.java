package no.experis.level;

//Stores all level related functions that need to be implemented
public interface Level<T>
{
    void SetLevel(T stats, int desiredLevel);
    void GiveExperience(T stats, int amount);
    void LevelUp(T stats);
    int CalculateLevel();
    int CalculateScaling();
}
