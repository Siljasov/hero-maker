package no.experis.level;

import no.experis.hero.HeroStats;

public class HeroLevel implements Level
{
    public int CurrentLevel = 1;
    public int ExperienceRequiredToLevel = 100;
    private int nextLevelUp = 100;
    public int ExperiencePoints = 0;
    public double ScalingPerLevel = 1.10;

    //Resets hero's level to it's default values and then levels up until it reaches the desired level
    @Override
    public void SetLevel(Object stats, int desiredLevel)
    {
        CurrentLevel = 1;
        ExperienceRequiredToLevel = 100;
        nextLevelUp = 100;
        ExperiencePoints = 0;
        for (int i = CurrentLevel; i < desiredLevel; i++)
        {
            LevelUp(stats);
        }
    }

    //Gives experience points to the hero. It does this by giving 1 EXP until it reaches the new total amount XP value
    @Override
    public void GiveExperience(Object stats, int amount)
    {
        int newExperienceAmount = amount + ExperiencePoints;
        do
        {
            if (ExperiencePoints >= ExperienceRequiredToLevel)
                LevelUp(stats);
            ExperiencePoints++;
        } while (newExperienceAmount > ExperiencePoints);
    }

    // Levels up a character once
    @Override
    public void LevelUp(Object stats)
    {
        ExperiencePoints = ExperienceRequiredToLevel;
        CurrentLevel = CalculateLevel();
        CalculateScaling();
        ((HeroStats)stats).ScaleUp();
    }

    // This function uses the same method as calculateScaling(), but starts from 0 and goes up to the latest XP requirement
    // to calculate what level the hero should be.
    @Override
    public int CalculateLevel()
    {
        int level = 1;
        int toNextLevel = 100;
        int requiredToLevel = 100;
        while(requiredToLevel <= ExperienceRequiredToLevel)
        {
            toNextLevel = (int)(toNextLevel * ScalingPerLevel);
            requiredToLevel = requiredToLevel + toNextLevel;
            level++;
        }
        return level;
    }

    //Calculates what XP amount the hero needs to get to the next level
    // XPRequired = (requirementForCurrentLevelWithoutTotal * 1.1) + currentTotalRequirement
    // For example level 2 requirement is: 210 = (100 * 1.1) + 100
    // then for level 3, the requirement is: 331 = (110 * 1.1) + 210
    // then for level 4 etc
    @Override
    public int CalculateScaling()
    {
        nextLevelUp = (int)(nextLevelUp * ScalingPerLevel);
        ExperienceRequiredToLevel = ExperienceRequiredToLevel + nextLevelUp;
        return ExperienceRequiredToLevel;
    }
}
