package no.experis.level;

import no.experis.items.weapon.WeaponStats;
//This class is responsible for all weapon level related tasks as well as keeping the level variable
public class WeaponLevel implements Level
{
    public int CurrentLevel = 1;

    //Resets a weapon to level 1 and then levels it up to the desired level
    @Override
    public void SetLevel(Object stats, int desiredLevel)
    {
        for (int i = 1; i < CurrentLevel; i++)
        {
            ((WeaponStats)stats).ScaleDown();
        }
        CurrentLevel = 1;
        for (int i = 1; i < desiredLevel; i++)
        {
            CurrentLevel++;
            ((WeaponStats)stats).ScaleUp();
        }
    }

    @Override
    public void GiveExperience(Object stats, int amount) { }

    //Levels up a weapon once
    @Override
    public void LevelUp(Object stats)
    {
        CurrentLevel++;
        ((WeaponStats)stats).ScaleUp();
    }

    @Override
    public int CalculateLevel()
    {
        return 0;
    }

    @Override
    public int CalculateScaling()
    {
        return 0;
    }
}
