package no.experis.level;

import no.experis.items.armor.ArmorStats;
//This class is responsible for all armor level related tasks as well as keeping the level variable
public class ArmorLevel implements Level
{
    public int CurrentLevel = 1;

    //Resets an item to level 1 and then levels it up to the desired level
    @Override
    public void SetLevel(Object stats, int desiredLevel)
    {
        for (int i = 1; i < CurrentLevel; i++)
        {
            ((ArmorStats)stats).ScaleDown();
        }
        CurrentLevel = 1;
        for (int i = 1; i < desiredLevel; i++)
        {
            CurrentLevel++;
            ((ArmorStats)stats).ScaleUp();
        }
    }

    @Override
    public void GiveExperience(Object stats, int amount) { }

    //Levels up an armor item once
    @Override
    public void LevelUp(Object stats)
    {
        CurrentLevel++;
        ((ArmorStats)stats).ScaleUp();
    }

    @Override
    public int CalculateLevel()
    {
        return CurrentLevel;
    }

    @Override
    public int CalculateScaling()
    {
        return 0;
    }
}
