# Hero Maker

Hero Maker is a simple console application written in Java that allows the user to create RPG characters. The application can create characters from three different classes, warrior, ranger and mage. After the character is created, the user can manipulate the character by adding new items and XP, and item XP which will alter the character's attributes. Hero Maker provides a quick and efficient way to create characters and test how various items and modifiers affect the character. 

## How to use

When the program is launched, it will draw the main menu which will consist of menu options that the user can pick from. Each number on the menu represents an action, and the user picks between those options.

# Class Structure
Below is a visualization of all the classes and how they are connected to each other to better understand how the program is structured.

*NOTE: A dotted line indicates that classes are related in some way to each other but do not inherit in any way*

![Diagram](https://gitlab.com/Siljasov/hero-maker/-/raw/master/RPG_Maker_Final.png)

[Click here to open the diagram](https://gitlab.com/Siljasov/hero-maker/-/raw/master/RPG_Maker_Final.png)
